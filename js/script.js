//                                       ДР №6
//                                       Теория

// 1. Экранирование – замена в тексте символов, управляющих структурой текста, 
// на соответствующие текстовые подстановки. Может потребоваться в ситуации, когда
// необходимо использовать управляющий символ как «обычный символ языка»,
// например, для поиска некоторых специальных символов [] \^ $. | ? * + ( );

// 2. Существует три способа объявления функции: Function Declaration, 
// Function Expression и Named Function Expression.

// 3. "Hoisting" - это подъем и перемещение интерпретатором JavaScript функций,
// переменных или классов в верхней части их области перед выполнением кода. 
// Подъем позволяет безопасно использовать функции в коде до объявления.

//    Практика
//                                        #1

function createNewUser() {
    const firstName = prompt("What is your name?");
    const lastName = prompt("What is your last name?");
    const birthday = prompt("Please enter your date of birth", "dd.mm.yyyy");
    const newUser = {
        firstName: firstName,
        lastName: lastName,
        birthday: birthday,
        getLogin() {
            return (this.firstName[0] + this.lastName).toLowerCase();
        },
        getAge: function () {
            const now = new Date();
            const birthDate = new Date(Number(this.birthday.slice(6)), Number(this.birthday.slice(3, 5) - 1), Number(this.birthday.slice(0, 2)))
            return Math.floor((now - birthDate) / 1000 / 31536000);
        },

        getPassword: function () {
            return (this.firstName[0].toLocaleUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(6));
        }
    }
    return newUser;
};
const user = createNewUser()
console.log("Object:", user);
console.log("Age:", user.getAge());
console.log("Password:", user.getPassword());


